using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_RotationSpeed;

    private Animator m_Animator;
    [SerializeField]
    private float m_Jump;
    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    private PlayerInput m_PlayerInput;
    private void Awake()
    {
        m_PlayerInput = new PlayerInput();

        //m_PlayerInput.Player.Jump.started += Jump;
        
        m_PlayerInput.Enable();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();

    }

    void Update()
    {
        m_Movement = Vector3.zero;
    }
    /*
    private void Jump(InputAction.CallbackContext context) 
    {
        Debug.Log("Jumping");
        m_Rigidbody.AddForce(Vector2.up * m_Jump, ForceMode.Impulse);
    }*/
    private void FixedUpdate()
    {
        Vector2 movement = m_PlayerInput.Player.Movement.ReadValue<Vector2>();
        if (movement.y > 0)
        {
            m_Animator.Play("Run_SwordShield");
            m_Movement += transform.forward;
        }
        else if (movement.y < 0)
            m_Movement -= transform.forward;
        /*
        //rotate
        if (movement.x > 0)
        {
            transform.Rotate(Vector2.up * m_RotationSpeed * Time.deltaTime);
        }
        else if (movement.x < 0)
            transform.Rotate(-Vector2.up * m_RotationSpeed * Time.deltaTime);
        */
        //strafe
        //rotate
        if (movement.x > 0)
        {
            //m_Movement += transform.right;
            transform.Rotate(Vector2.up * m_RotationSpeed * Time.deltaTime);
        }
        else if (movement.x < 0) 
        {
            //m_Movement -= transform.right;
            transform.Rotate(-Vector2.up * m_RotationSpeed * Time.deltaTime);
        }

        m_Movement.Normalize();
        m_Rigidbody.MovePosition(transform.position + m_Movement * m_Speed * Time.fixedDeltaTime);
   
    }


}
