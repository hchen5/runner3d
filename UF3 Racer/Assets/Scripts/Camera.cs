using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{

    [SerializeField]
    private GameObject m_Player;

    [SerializeField]
    private Vector3 m_Offset; // direccion posicion
    //private int m_Index = 0; // si tienes mas de 1 objetivo
    [Range(0.1f,1f)]
    [SerializeField]
    private float m_smooth =0.5f;

    private bool rotation = true;
    [SerializeField]
    private float rotationspeed =5f;

   
    //private bool lookPlayer = false;
    private void Start()
    {
        Cursor.lockState= CursorLockMode.Locked; //lockea el mouse dentro del game 
       
    }
    private void LateUpdate()
    {
        Quaternion camAngleX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationspeed, Vector3.up); // angulo de rotacion del mouse X (horizontal)
        Quaternion camAngleY = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * rotationspeed, Vector3.right); // angulo de rotacion del mouse y (vertical)


        m_Offset = (camAngleX * camAngleY).normalized * m_Offset;

        //}
        Vector3 posPlayer = m_Player.transform.position + m_Offset; //la nueva posicion del pj


        transform.position = Vector3.Slerp(transform.position, posPlayer, m_smooth); //decir que la posicion de la camara sea el vector3 i hacemos slerp para interpolar esfericamente 2 posiciones
        transform.rotation = Quaternion.LookRotation(m_Player.transform.position - transform.position);   //rotar sobre la posicion del personaje        
    
    }
   

    

    /*
    [SerializeField]
    private GameObject[] m_Target;

    private int m_Index = 0;

    [SerializeField]
    private Vector3 m_Offset;

    void Update()
    {
        transform.position = m_Target[m_Index].transform.position + m_Target[m_Index].transform.forward * m_Offset.z + m_Target[m_Index].transform.up * m_Offset.y;
        transform.LookAt(m_Target[m_Index].transform.position);

        if (Input.GetKeyDown(KeyCode.C))
            m_Index = (m_Index + 1) % m_Target.Length;
    }*/

}
