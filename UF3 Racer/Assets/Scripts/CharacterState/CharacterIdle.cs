using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using State = FiniteStateMachine.State;

public class CharacterIdle : State
{
    CharacterBehaviuor m_characterBehaviuor;
    string m_IdleAnimations;
    public CharacterIdle(FSM fsm,string animation) :base(fsm)
    {
        m_characterBehaviuor =fsm.Owner.GetComponent<CharacterBehaviuor>();
        m_IdleAnimations = animation;
    }
    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_IdleAnimations);
        m_characterBehaviuor.m_PlayerInput.Player.Jump.performed += StartedJumping;
        
    }
    public override void Exit()
    {
        base.Exit();
        m_characterBehaviuor.m_PlayerInput.Player.Jump.performed -= StartedJumping;
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<CharacterJump>();


    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_characterBehaviuor.m_PlayerInput.Player.Movement.ReadValue<Vector2>().y != 0)
        {
            m_FSM.ChangeState<CharacterRun>();
        }

        if (m_characterBehaviuor.m_PlayerInput.Player.Movement.ReadValue<Vector2>().x != 0)
        {
            m_FSM.ChangeState<CharacterRotate>();
        }
    }
}
