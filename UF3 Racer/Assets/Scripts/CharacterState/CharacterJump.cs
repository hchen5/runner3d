using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJump : CharacterMovement
{
    float m_JumpForce;
    public CharacterJump(FSM fSM, string animation, float speed, float m_rotationSpeed) : base(fSM, animation, speed, m_rotationSpeed)
    {
        m_JumpForce = speed;
    }
    public override void Init()
    {
        base.Init();
        if(m_CharacterBehaviuor.isGrounded())
        m_RigidBody.AddForce(Vector2.up * m_JumpForce, ForceMode.Impulse);
    }
}
