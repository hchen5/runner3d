using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterRun : CharacterMovement
{
    string m_IdleAnimations;
    public CharacterRun(FSM fSM, string animation, float speed ,float rotation  )
        : base(fSM, animation, speed,rotation)
    {
        m_IdleAnimations= animation;
    }

    public override void Init() 
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_IdleAnimations);
        m_CharacterBehaviuor.m_PlayerInput.Player.Jump.performed += StartedJumping;
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<CharacterJump>();

        
    }
    public override void Exit()
    {
        base.Exit();
        m_CharacterBehaviuor.m_PlayerInput.Player.Jump.performed -= StartedJumping;
    }
    
}
