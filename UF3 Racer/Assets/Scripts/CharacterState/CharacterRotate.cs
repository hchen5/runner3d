using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotate : CharacterMovement
{
    public CharacterRotate(FSM fSM, string animation, float speed, float m_rotationSpeed ) 
        : base(fSM, animation, speed, m_rotationSpeed)
    {
    }
    public override void Init()
    {
        base.Init();

    }

    public override void Exit()
    {
        base.Exit();
    }

}
