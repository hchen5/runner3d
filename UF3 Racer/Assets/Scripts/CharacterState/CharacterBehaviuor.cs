using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterBehaviuor : MonoBehaviour
{
    FSM m_FSM;
    PlayerInput m_Inputs;
    public PlayerInput m_PlayerInput => m_Inputs;
    

    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_RotationSpeed;

    private Animator m_Animator;
    [SerializeField]
    private float m_Jump;
    //protected Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;
    public bool ground;
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
        m_Inputs = new PlayerInput();
        m_Inputs.Player.Enable();
        //m_PlayerInput.Player.Jump.started += Jump;

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new CharacterIdle(m_FSM, "Idle_SwordSHield"));
        m_FSM.AddState(new CharacterRun(m_FSM, "Run_SwordShield",m_Speed ,m_RotationSpeed ));
        m_FSM.AddState(new CharacterRotate(m_FSM ,"",m_Speed,m_RotationSpeed ));
        m_FSM.AddState(new CharacterJump(m_FSM, "",m_Jump,m_RotationSpeed));
        m_FSM.ChangeState<CharacterIdle>();
    }
    void Update()
    {
        m_FSM.Update();
        
        
    }
    /*
    public bool CheckGround()
    {
        //transform.GetComponent<SphereCollider>().radius * 1.1f
        bool raycast = Physics.Raycast(transform.position , -transform.up , this.GetComponent<SphereCollider>().radius *1.2f);
        //RaycastHit hit ;
        Debug.DrawRay(this.GetComponent<SphereCollider>().transform.position , -(transform.up *0.5f)  ,Color.red ,3f);
        print(raycast+"bjxacvadswhbj");
        //Debug.DrawLine(hit.point,Color.red);
        return raycast; 
    }*/
    public bool isGrounded() 
    {
        Ray ray = new Ray(transform.position +Vector3.up *0.25f, Vector3.down);
        Debug.DrawRay(ray.origin ,ray.direction,Color.red,3f);
        //Debug.DrawLine(transform.position+Vector3.up *0.25f,Vector3.down ,Color.red,2f);
        if (Physics.Raycast(ray, out RaycastHit hit, 0.3f))
        { 
            return true;
        }
        else
            return false;

    }
    /*
    public void Jump(InputAction.CallbackContext context)
    {
        
        if (isGrounded())
        {
            Debug.Log("Jumping");
            m_Rigidbody.AddForce(Vector2.up * m_Jump, ForceMode.Impulse);
        }
        
    }*/
    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }
    /*
    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
        Vector2 movement = m_PlayerInput.Player.Movement.ReadValue<Vector2>();
        if (movement.y > 0)
        {
            m_Animator.Play("Run_SwordShield");
            m_Movement += transform.forward;
        }
        else if (movement.y < 0)
            m_Movement -= transform.forward;
       
        //strafe
        //or
        //rotate
        if (movement.x > 0)
        {
            //m_Movement += transform.right; //strafe
            transform.Rotate(Vector2.up * m_RotationSpeed * Time.deltaTime);//rotate
        }
        else if (movement.x < 0)
        {
            //m_Movement -= transform.right; 
            transform.Rotate(-Vector2.up * m_RotationSpeed * Time.deltaTime);
        }

        m_Movement.Normalize();
        m_Rigidbody.MovePosition(transform.position + m_Movement * m_Speed * Time.fixedDeltaTime);

    }
   */

}
