using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class CharacterMovement : State
{
    protected CharacterBehaviuor m_CharacterBehaviuor;
    protected Rigidbody m_RigidBody;
    protected string m_MovementAnimation;
    protected float m_MovementSpeed;
    protected float m_RotationSpeed;
    protected Vector3 m_Movement =Vector3.zero;
    
    public CharacterMovement(FSM fSM ,string animation,float speed ,float m_rotationSpeed ) : base(fSM)
    {
        m_CharacterBehaviuor = fSM.Owner.GetComponent<CharacterBehaviuor>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();
        m_MovementAnimation = animation;
        m_MovementSpeed = speed;
        m_RotationSpeed = m_rotationSpeed;
        
    }

    public override void Init()
    {
        base.Init();
        m_CharacterBehaviuor.m_PlayerInput.Player.Movement.canceled += StopMovement;
        m_CharacterBehaviuor.m_PlayerInput.Player.Jump.performed += StartedJumping;
    }

    public override void Exit()
    {
        base.Exit();
        m_CharacterBehaviuor.m_PlayerInput.Player.Movement.canceled -= StopMovement;
        m_CharacterBehaviuor.m_PlayerInput.Player.Jump.performed -= StartedJumping;
    }
    private void StopMovement(InputAction.CallbackContext context)
    {
        m_RigidBody.velocity = new Vector2(0, m_RigidBody.velocity.y);
        if (m_RigidBody.velocity.y == 0 && m_RigidBody.velocity.x==0)
            m_FSM.ChangeState<CharacterIdle>();
        //else
        // m_FSM.ChangeState<CharacterJump>();
        
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<CharacterJump>();


    }
    public override void Update() 
    {
        //m_FSM.Update();
        m_Movement = Vector3.zero;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        //float verticalsAxis = m_CharacterBehaviuor.m_PlayerInput.Player.Movement.ReadValue<Vector2>().y;
        Vector2 movement = m_CharacterBehaviuor.m_PlayerInput.Player.Movement.ReadValue<Vector2>();
        if (movement.y != 0f)
        {
            if (movement.y > 0)
            {
                // m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, m_RigidBody.velocity.y, m_MovementSpeed);
                //m_RigidBody.transform.Rotate(-Vector2.up * m_RotationSpeed * Time.deltaTime);
                m_Movement += m_CharacterBehaviuor.transform.forward;
            }
            else
            {
                //m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, m_RigidBody.velocity.y, -m_MovementSpeed);
                m_Movement -= m_CharacterBehaviuor.transform.forward;

            }
        }
        //float horizontalAxis = m_CharacterBehaviuor.m_PlayerInput.Player.Movement.ReadValue<Vector2>().x;
        if (movement.x != 0f)
        {
            if (movement.x > 0)
            {
                m_RigidBody.transform.Rotate(Vector2.up * m_RotationSpeed * Time.deltaTime);

            }
            else
            {
                m_RigidBody.transform.Rotate(-Vector2.up * m_RotationSpeed * Time.deltaTime);

            }
        }

        m_Movement.Normalize();
        //m_Rigidbody.MovePosition(transform.position + m_Movement * m_Speed * Time.fixedDeltaTime);
        m_RigidBody.MovePosition(m_CharacterBehaviuor.transform.position + m_Movement * m_MovementSpeed *Time.fixedDeltaTime);
    }
}
